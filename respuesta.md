# Protocolos para la transmisión de audio y video en Internet
# Práctica 5. Sesión SIP

## Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura?
    * Componen la captura 1050 paquetes. Estos paquetes están numerados, por lo tanto en el último paquete de la captura podemos observar el numero total. También podriamos verlo en el campo Stadistics/Protocol Hierarchy.
* ¿Cuántos paquetes componen la captura?
    * Componen la captura 1050 paquetes. Estos paquetes están numerados, por lo tanto en el último paquete de la captura podemos observar el numero total. También podriamos verlo en el campo Stadistics/Protocol Hierarchy.
* ¿Cuánto tiempo dura la captura?
    * En el ultimo paquete, en el campo frame 1090/Time since reference or first frame/ podemos ver la duración de la captura: 10,521629 segundos.
    Además, lo podemos encontrar a la izquierda en el último paquete.
* ¿Qué IP tiene la máquina donde se ha efectuado la captura?
    * La IP es la 192.168.1.116
* ¿Se trata de una IP pública o de una IP privada?
    * Es una IP privada
* ¿Por qué lo sabes?
    * Lo sabemos ya que esta entre 192.168.0.0 y 192.168.255.255, debido a que es el rango de las privadas de clase C(ipv4).

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
    * UDP, SIP y RTP.
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
    * IPV4, ICMP, UDP, Ethernet
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo?¿Cuánto es ese tráfico?
    * El UDP con 134k bytes/seg
 
* ¿En qué segundos tienen lugar los dos primeros envíos SIP?
    * En el segundo 0
* Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
    * Apartir del paquete 6.
* Los paquetes RTP, ¿cada cuánto se envían?
    * Cada 0,01 seg.

## Ejercicio 4. Primeras tramas

A* ¿De qué protocolo de nivel de aplicación son?
    * SIP
* ¿Cuál es la dirección IP de la máquina "Linphone"?
    * 192.168.1.116
* ¿Cuál es la dirección IP de la máquina "Servidor"?
    * 212.79.111.155
* ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
    * Es un request con la cabecera Invite para conectar con el servidor.
* ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
    * En esta trama se responde con un trying, para informar de que se está intentando esablecer comunicación.
* ¿De qué protocolo de nivel de aplicación son?
    * SIP
* ¿Entre qué máquinas se envía cada trama?
    * Entre servidor y Linphone y luego al revés.
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
    * Servidor establece comunicacion, con el 200 OK.
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
    * El linphone envia al Server un ACK, para comunicarle que ya les ha llegado la confirmación de la conexión.

## Ejercicio 5. Tramas finales


Después de la trama 250, busca la primera trama SIP.

* ¿Qué número de trama es?
    * 1042
* ¿De qué máquina a qué máquina va?
    * Linphone a Server
* ¿Para qué sirve?
    * Para terminar la comunicacion
* ¿Puedes localizar en ella qué versión de Linphone se está usando?
    * La versión Linphone Desktop/4.3.2 

## Ejercicio 6. Invitación

Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

* ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
    * music@sip.iptel.org
* ¿Qué instrucciones SIP entiende el UA?
    * INVITE, ACK, CANCEL, UPDATE, OPTIONS, MESSAGE, BYE, REFER, NOTIFY, SUBSCRIBE, INFO, PRACK.
* ¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?
    * En sip, Messager header : Content-type

* ¿Cuál es el nombre de la sesión SIP?
    * El nombre es Talk. Lo podemos encontrar en SIP/Message body/SDP/Session name.

## Ejercicio 7. Indicación de comienzo de conversación
En la propuesta SDP de Linphone puede verse un campo `m` con un valor que empieza por `audio 7078`.

* ¿Qué trama lleva esta propuesta?
    * La segunda
* ¿Qué indica el `7078`?
    * Es el puerto de transporte
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
    * Es donde se recibirán o enviarán los paquetes
* ¿Qué paquetes son esos?
    * Son paquetes RTP
En la respuesta a esta propuesta vemos un campo `m` con un valor que empieza por `audio XXX`.
* ¿Qué trama lleva esta respuesta?
    * La cuarta
* ¿Qué valor es el `XXX`?
    * 29448
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
    * Es donde se recibirán o enviarán los paquetes
* ¿Qué paquetes son esos?
    * Paquetes RTP


## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

* ¿De qué máquina a qué máquina va?
    * De Linphone a Server
* ¿Qué tipo de datos transporta?
    * El tipo de datos que transporta es multimedia
* ¿Qué tamaño tiene?
    * El tamaño es de 214 bytes
* ¿Cuántos bits van en la "carga de pago" (payload)
    * 1280 bits
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
    * Se envia un paquete RTP cada 0.01 seg.


## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?
    * Hay dos flujos, el primero cliente --> servidor y el segundo servidor --> cliente
* ¿Cuántos paquetes se pierden?
    * Ninguno
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
    * El valor máximo del delta es 30,73 ms.
* ¿Qué es lo que significa el valor de delta?
    * Lo que significa es la diferencia de tiempo con el paquete anterior, es decir, la diferencia de tiempo entre los envios de paquetes.
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
    * En el flujo desde el servidor al cliente.
* ¿Qué significan esos valores?
    * Es la variabilidad que hay entre retardos.

* ¿Cuánto valen el delta y el jitter para ese paquete?
    * Delta: 59.581866 ms y Jitter: 7.110079 ms.
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
    * Si, mirando los numeros de la secuencia, si no estan en orden es debido a que algun paquete se ha retrasado. También podriamos mirar el valor de skew.
* El "skew" es negativo, ¿qué quiere decir eso?
    * Que el paquete se ha retrasado

* ¿Qué se oye al pulsar `play`?
    * Una señal de audio 
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?
    * Disminuye la calidad de la llamada, debido a que se pierde informacion por tener un jitter buffer tan pequeño.
* ¿A qué se debe la diferencia?
    * Esto se debe al retraso en la llegada de paquetes.


 ## Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` selecciona el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?
    * Dura un poco mas que 10 sg
* ¿En qué segundo se recibe el último OK que marca el final de la llamada?
    * En el segundo 10,52
  
Ahora, selecciona los dos streams, y pulsa sobre `Play Sterams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
    * El cliente: 0xD2DB8B4 y el servidor: 0x5C44A34B.
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
    * 514
* ¿Cuál es la frecuencia de muestreo del audio?
    * 8 KHz
* ¿Qué formato se usa para los paquetes de audio (payload)?
    * g711U.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 11. Captura de una llamada VoIP

Desde LinPhone, créate una cuenta SIP.  A continuación:

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 10 segundos de duración. Comienza a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Asegura (usando los filtros antes de guardarla) que en la captura haya solo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos RTP tiene esta captura?
    * 2
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?
    * Primer flujo:
        * Valor máximo del delta : 31.7
        * valor medio jitter: 0.79
        * valor máximo jitter: 2.62
    * Segundo flujo:
        * Valor máximo del delta: 32.78
        * valor medio jitter: 5.15
        * valor máximo jitter: 5.95

## Ejercicio 12 (segundo periodo). Llamada LinPhone

Ponte de acuerdo con algún compañero para realizar una llamada SIP conjunta (entre los dos) usando LinPhone, de unos 15 segundos de duración.  Si quieres, usa el foro de la asignatura, en el hilo correspondiente a este ejercicio, para enconrar con quién hacer la llamada.

Realiza una captura de esa llamada en la que estén todos los paquetes SIP y RTP (y sólo los paquetes SIP y RTP). No hace falta que tu compañero realice captura (pero puede realizarla si quiere que le sirva también para este ejercicio).

Guarda tu captura en el fichero `captura-a-dos.pcapng`

Como respuesta a este ejercicio, indica con quién has realizado la llamada, quién de los dos la ha iniciado, y cuantos paquetes hay en la captura que has realizado.
